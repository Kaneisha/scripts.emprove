var wellsFargo = '2';
//var wellsFargo = '27';

/**************************************************************************
 * 
 * Submits a credit application to Wells Fargo or BOA.
 * This event is triggered when a user clicks the 'Credit Application' button
 * on the customer record (in view mode)
 * Updates the fields to let user know the status of the application.
 * 
 ****************************************************************************/
function onCreditApp_Click(){
	try{
//		alert('hello world');
		var context = nlapiGetContext();
		var transaction = {};
		var customer = {};
		var so = {};
		var credentials = {};
		
		var nsCustomer = nlapiLoadRecord('customer', nlapiGetRecordId());
		customer = populateCustomer(nsCustomer,context);
		
		credentials.userName = context.getPreference('custscript_wf_credit_app_username');  //get it from the script record
		credentials.password = context.getPreference('custscript_wf_credit_app_password');
		credentials.merchantNumber = context.getPreference('custscript_wf_merchant_number');
		credentials.url = context.getPreference('custscript_wf_credit_application_url');
		
		
		transaction.credentials = credentials;
		transaction.customer = customer;

		var returnObject = WFCreditApplication(transaction);
		alert(returnObject.transactionStatus);
		
		
		if(returnObject.approved){
			var fields = ['custentity_account_number', 'custentity_transaction_status','custentity_auth_number'];
			var values = [returnObject.accountNumber, returnObject.transactionStatus, returnObject.authNumber];
			nlapiSubmitField('customer', nlapiGetRecordId(), fields, values);
			//alert('Customer Id - ' + nlapiGetRecordId());
		}else{
			//alert('Denied');
			var fields = ['custentity_transaction_status'];
			var values = [returnObject.transactionStatus];
			nlapiSubmitField('customer', nlapiGetRecordId(), fields, values);
		}
		
		updateActivityLog(nlapiGetRecordId(),returnObject.transactionStatus,nsCustomer.getFieldValue('terms'));
		location.reload();
		
		
	}catch(e){
		alert('Error - ' + e.message);
		nlapiLogExecution('Debug', 'onCreditApp_Click', 'Error Message - ' + e.message);
	}
	
}

/**************************************************************************
 * 
 * Submits a request to check for an account to Wells Fargo or BOA.
 * This event is triggered when a user clicks the 'Verify Account' button
 * on the customer record (in view mode)
 * 
 ****************************************************************************/
function onVerifyAccount_Click(){
	try{
		nlapiLogExecution('Debug', 'onVerifyAccount_Click', 'Button Clicked');
		
		var context = nlapiGetContext();
		var transaction = {};
		var customer = {};
		var so = {};
		var credentials = {};
		
		var nsCustomer = nlapiLoadRecord('customer', nlapiGetRecordId());
		customer = populateCustomer(nsCustomer,context);
		
		credentials.userName = context.getPreference('custscript_wf_credit_app_username');  //get it from the script record
		credentials.password = context.getPreference('custscript_wf_credit_app_password');
		credentials.merchantNumber = context.getPreference('custscript_wf_merchant_number');
		credentials.url = context.getPreference('custscript_wf_inquiry_service_url');
		
		transaction.credentials = credentials;
		transaction.customer = customer;
		
		
		var returnObject = WFSubmitInquiry(transaction);
		
		if(returnObject.approved){
			alert('Account Found, ' + returnObject.transactionStatus);
		}else{
			alert(returnObject.transactionStatus);
		}
	}catch(e){
		alert('Verify account error - ' + e.message);
		nlapiLogExecution('Debug', 'onVerifyAccount_Click', 'Error Message - ' + e.message);
	}
	
}


/**************************************************************************
 * 
 * Adds Credit Application and Verify Account button to the customer record
 * when it's in view mode.
 * Verify Account only appears if there is an account number on the record
 * 
 * Grabs the value of the social security number and masks the field
 * 
 ****************************************************************************/
function onBeforeLoad(type, form){
	try{
		var record = nlapiGetNewRecord();
		var recType = record.getRecordType();
		var ssn = record.getFieldValue('custentity_social_security_number');
		var jointSSN = record.getFieldValue('custentity_joint_app_ssn');
		var maskSSN = record.getFieldValue('custentity_masked_ssn');
//		nlapiLogExecution('Debug', 'onBeforeLoad', 'SSN Number - ' + ssn);
//		nlapiLogExecution('Debug', 'onBeforeLoad', 'Mask SSN Number - ' + maskSSN);
		
		//All credit functions are available in view mode only.  To make sure that all data as already been entered on the form
		if(recType == 'customer'){
			if (type == 'view'){
				form.setScript('customscript_credit_processing_cs');
				var applyBtn = form.addButton('custpage_applybtn', 'Apply For Credit', 'onCreditApp_Click()');
				var account = nlapiGetFieldValue('custentity_account_number');
				
				if(account != null){
					var verifyBtn = form.addButton('custpage_verifybtn', 'Verify Account', 'onVerifyAccount_Click()');
				}
			}

		}
	}catch(e){
		nlapiLogExecution('Error', 'onBeforeLoad', 'Unexpected Error - ' + e.message);
	}
	
}

/**************************************************************************
 * 
 * If the record is an invoice or sales order, sends an alert to fill out
 * the plan number and transaction code fields if the terms field is Wells
 * Fargo
 * 
 ****************************************************************************/
function onSave(){
	var recType = nlapiGetRecordType();
	var planNumber = nlapiGetFieldValue('custbody_credit_app_plan_number');
	var transCode = nlapiGetFieldValue('custbody_transaction_code');
	var override = nlapiGetFieldValue('custbody_manual_override');
	
	try{
		if(recType == 'salesorder' || recType == 'invoice'){
			var terms = nlapiGetFieldValue('terms');
			nlapiLogExecution('Debug', 'onSave', 'Terms - ' + terms);
			nlapiLogExecution('Debug', 'onSave', 'Plan Number - ' + planNumber);
			nlapiLogExecution('Debug', 'onSave', 'Transaction Code - ' + transCode);
			
			var orderTotal = nlapiGetFieldValue('total');
			planNumber = getPlan(orderTotal);
			nlapiSetFieldValue('custbody_credit_app_plan_number',planNumber);
			nlapiSetFieldValue('custbody_em_trans_type',recType);
			nlapiLogExecution('Debug', 'onSave', 'Plan - ' + planNumber);
			
			if(terms == wellsFargo && override == 'F'){
				var c = confirm('Did you want to send this transaction to Wells Fargo? If so press Ok');
				if(c == true){
					if(isEmptyString(planNumber)){
						nlapiLogExecution('Debug', 'onSave', 'Inside Empty Plan Number');
						alert('Please enter a Plan Number');
						return false;
					}
					if(isEmptyString(transCode)){
						nlapiLogExecution('Debug', 'onSave', 'Inside Empty Trans Code');
						alert('Please enter a Transaction Code');
						return false;
					}
					
					nlapiSetFieldValue('custbody_send_to_wf','T');
					
					
				}
				if(c == false){
					return true;
				}
			}
		}
		
	}catch(e){
		nlapiLogExecution('Error', 'onSave', 'Unexpected Error - ' + e.message);
	}
	
	return true;
}

/**************************************************************************
 * 
 * Grabs the value on the Sales Order and sends them to the WFSubmitTransaction
 * function to submit the transaction to Wells Fargo.
 * Updates the fields to let user know the status of the transaction.
 * 
 ****************************************************************************/
function onBeforeSubmit(){
	try{
		nlapiLogExecution('Debug', 'onBeforeSubmit', 'Before Submit Triggered');
		var context = nlapiGetContext();
		if(context.getExecutionContext() == 'userevent'){
			return;
		}
		var record = nlapiGetNewRecord();
		var recType = record.getRecordType();

		if(recType == 'customer'){			
			var ssnSession = context.getSessionObject('SSN');
			//nlapiLogExecution('Debug', 'onBeforeSubmit', 'Social - ' + ssnSession);
			nlapiLogExecution('Debug', 'onBeforeSubmit', 'Joint Indicator - ' + record.getFieldValue('custentity_individual_joint_indicator'));//custentity_individual_joint_indicator

			if(ssnSession == null){
				//var jointssnSession = context.getSessionObject('jointSSN');
				var social = record.getFieldValue('custentity_masked_ssn');
				//nlapiLogExecution('Debug', 'onBeforeSubmit', 'Inside ssnSession - ' + social);
				context.setSessionObject('SSN', social);
				
			}
			var jointssnSession = context.getSessionObject('jointSSN');
			//nlapiLogExecution('Debug', 'onBeforeSubmit', 'Joint Social - ' + jointssnSession);
			if(record.getFieldValue('custentity_individual_joint_indicator') == 'T' && jointssnSession == null){
				nlapiLogExecution('Debug', 'onBeforeSubmit', 'Inside Joint Session');
//				context.setSessionObject('SSN', social);
				var jointSocial = record.getFieldValue('custentity_joint_masked_ssn');
				context.setSessionObject('jointSSN', jointSocial);
			}
			
			record.setFieldValue('custentity_masked_ssn', '');
			record.setFieldValue('custentity_joint_masked_ssn', '');

		}
		
		var send = record.getFieldValue('custbody_send_to_wf');
		nlapiLogExecution('Debug', 'onBeforeSubmit', 'Send - ' + send);
		
		if(send == 'T'){
			
			nlapiLogExecution('Debug', 'onBeforeSubmit', 'Inside Send Is True');
			var session = context.getSessionObject('CREDIT_CHECK');
			var transaction = {};
		//	var customer = {};
			var so = {};
			var credentials = {};
			var transCode = record.getFieldValue('custbody_transaction_code');
			var terms = record.getFieldValue('terms');
			nlapiLogExecution('Debug', 'onBeforeSubmit', 'session - ' + session);
			
		//nlapiLogExecution('Debug', 'onBeforeSubmit', 'transCode - ' + transCode);
		//nlapiLogExecution('Debug', 'onBeforeSubmit', 'terms - ' + terms);
			if(!isEmptyString(transCode)){
			if(recType == 'salesorder' && transCode == '1' || recType == 'salesorder' && transCode == '3' || recType == 'invoice' && transCode == '1' || recType == 'invoice' && transCode == '3'){
				
				var nsSalesOrder = nlapiGetNewRecord();
				so.amount = nsSalesOrder.getFieldValue('total');
				so.authNumber = nsSalesOrder.getFieldValue('custbody_authorization_number');
				so.planNumber = nsSalesOrder.getFieldValue('custbody_credit_app_plan_number');
				so.transCode = nsSalesOrder.getFieldValue('custbody_transaction_code');
				so.accountNumber = nsSalesOrder.getFieldValue('custbody_account_number');
				so.override = nsSalesOrder.getFieldValue('custbody_manual_override');
				so.tranid = nsSalesOrder.getFieldValue('tranid');
				
				if(so.transCode == '1'){
					so.authNumber = '000000';
				}
				else if(so.transCode == '2'){
					so.transCode = '3';
					so.authNumber = nsSalesOrder.getFieldValue('custbody_authorization_number');
				}
				else if(so.transCode == '3'){
					so.transCode = '5';
					so.authNumber = '000000';
				}
				
				nlapiLogExecution('Debug', 'onBeforeSubmit', 'Auth Number Before Response - ' + so.authNumber);
				
				transaction.so = so;
				nlapiLogExecution('Debug', 'onBeforeSubmit', 'Object - ' + JSON.stringify(so));
				/*
				var nsCustomer = nlapiLoadCustomer(nsSalesOrder.getFieldValue('entity'));
				customer = populateCustomer(nsCustomer);
				*/
			
			
				credentials.userName = context.getPreference('custscript_wf_credit_app_username'); 
				credentials.password = context.getPreference('custscript_wf_credit_app_password');
				credentials.merchantNumber = context.getPreference('custscript_wf_merchant_number');
				credentials.url = context.getPreference('custscript_wf_submit_transaction_url');
				transaction.credentials = credentials;
				nlapiLogExecution('Debug', 'onBeforeSubmit','credentials.url - ' + credentials.url);
				var returnObject = WFSubmitTransaction(transaction);
				
				nlapiLogExecution('Debug', 'onBeforeSubmit','returnObject - ' + JSON.stringify(returnObject));
				nlapiLogExecution('Debug', 'onBeforeSubmit','returnObject.transactionStatus - ' + returnObject.transactionStatus);
				nlapiLogExecution('Debug', 'onBeforeSubmit', 'Transaction Status - ' + returnObject.transactionStatus);
				nlapiLogExecution('Debug', 'onBeforeSubmit', 'Auth Number - ' + returnObject.authNumber);
				nlapiLogExecution('Debug', 'onBeforeSubmit', 'Trans Id - ' + returnObject.transactionStatusId);
		
				record.setFieldValue('custbody_transaction_status', returnObject.transactionStatus);
				record.setFieldValue('custbody_authorization_number', returnObject.authNumber);
				
				
				if(returnObject.transactionStatusId == 'A1'){
					nlapiLogExecution('Debug', 'onBeforeSubmit', 'Inside Approved Session');
					context.setSessionObject('CREDIT_CHECK', 'Approved');
					nsSalesOrder.setFieldValue('custbody_transaction_code', null);
				}

				updateTransActivityLog(nlapiGetRecordId(),returnObject.transactionStatus,terms);
	
			}
			}
			
			record.setFieldValue('custbody_send_to_wf','F');
			
		}
		
	}catch(e){
		alert(e.message);
		nlapiLogExecution('Error', 'onBeforeSubmit', 'Unexpected Error - ' + e.message);
	}
}

/**************************************************************************
 * 
 * If the type is invoice and the transaction code is authorize or
 * authorize and charge, send the invoice to Wells Fargo and then
 * transform the invoice to a customer payment.
 * 
 ****************************************************************************/
function onAfterSubmit(){
	var record = nlapiGetNewRecord();
	var recType = record.getRecordType();
	var ctx = nlapiGetContext();
	var session = ctx.getSessionObject('CREDIT_CHECK');
	nlapiLogExecution('Debug', 'onAfterSubmit', 'session - ' + session);
	
	if(recType == 'invoice'){
			
			if(session == 'Approved'){
				nlapiLogExecution('Debug', 'onAfterSubmit', 'id - ' + record.getId());
				var department = record.getFieldValue('department');
				var invoiceClass = record.getFieldValue('class');
				var transCode = record.getFieldValue('custbody_transaction_code');
				var status = record.getFieldValue('custbody_transaction_status');
				var salesRep = record.getFieldValue('salesrep');
				var terms = record.getFieldValue('terms');
				
				nlapiLogExecution('Debug', 'onAfterSubmit', 'Transaction Code - ' + transCode);
				try{
					nlapiLogExecution('Debug', 'onAfterSubmit', 'Inside if approved statement');
					ctx.setSessionObject('CREDIT_CHECK', null);
					nlapiLogExecution('Debug', 'onAfterSubmit', 'before record transform');
					var customerPayment = nlapiTransformRecord('invoice', record.getId(), 'customerpayment');
					customerPayment.setFieldValue('department', department);
					customerPayment.setFieldValue('class', invoiceClass);
	//				customerPayment.setFieldValue('checknum', '1234');
					customerPayment.setFieldValue('paymentmethod', '9');
					customerPayment.setFieldValue('custbodyrep', '2');
							
					var submit = nlapiSubmitRecord(customerPayment, true);
					nlapiSetRedirectURL('RECORD', 'customerpayment', submit, true);
	
				}catch(e){
					nlapiLogExecution('Error', 'onAfterSubmit', 'Error Message - ' + e.message);
				}
			}
	}
}


function getPlan(orderTotal){

//	var orderTotal = 3455;
	nlapiLogExecution('Debug', 'getPlan', 'orderTotal - ' + orderTotal);
	var plans = [{plan:'4020', 'min':2399.00},
	             {plan:'1064', 'min':1799.00},
	             {plan:'1046', 'min':1499.00},
	             {plan:'1015', 'min':1199.00},
	             {plan:'1145', 'min':499.00}
	             ]


	for(var x=0; x<plans.length; x++){
	    if (orderTotal >= plans[x].min){
	    	return  plans[x].plan;
	    }


	}
	
	if(orderTotal < plans[plans.length-1].min){
	  	return plans[plans.length-1].plan;
	}
	
	return '';

}

function isEmptyString(aValue){
	//If the values are empty or null run the function
	if(aValue == '' || aValue == null){
		return true;
	}
	
	return false;
}

