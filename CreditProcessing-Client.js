/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 */
define(['N/record','/SuiteScripts/emprove','/SuiteScripts/CreditProcessingController-2_0'], 
    function(record,emprove,creditProcessor) {

        function applyForCredit_onClick(id,term){
			try{
				var financeModule = emprove.getFinanceOrgModule(term,id,'creditApp');
	            log.debug('applyForCredit_onClick','financeModule - ' + financeModule);
	            creditProcessor.CreditApp(financeModule,id);
	            //alert(financeModule);
			}catch(e){
				log.error('applyForCredit_onClick','error - ' + e.message);
				alert('error - ' + e.message);
			}

        }
        
		function onVerifyAccount_Click(id,term){
			try{
				var financeModule = emprove.getFinanceOrgModule(term,id,'verifyAccount');
				creditProcessor.VerifyAccount(financeModule,id);
			}catch(e){
				alert('error - ' + e.message);
			}
			
		}
		
       function saveRec(context) {
    	    
    		try{
    			var record = context.currentRecord;
    			if(record.type == 'salesorder' || record.type == 'invoice'){
    				var terms = record.getValue('terms');
    				var ef = emprove.init();
	            	log.debug('saveRec', 'ef - ' + ef);
	            	ef = emprove.useFinance(terms);
	            	log.debug('saveRec', 'ef - ' + ef);
	            	
	            	if(ef){
	            		var planNumber = record.getValue('custbody_credit_app_plan_number');
	            		var transCode = record.getValue('custbody_transaction_code');
	            		var override = record.getValue('custbody_manual_override');
	       
	    				log.debug('saveRec', 'Terms - ' + terms);
	    				log.debug('saveRec', 'Plan Number - ' + planNumber);
	    				log.debug('saveRec', 'Transaction Code - ' + transCode);
	    				log.debug('saveRec', 'Override - ' + override);
	    				
	    				var orderTotal = record.getValue('total');
	    				planNumber = getPlan(orderTotal);
	    				record.setValue('custbody_credit_app_plan_number',planNumber);
	    				record.setValue('custbody_em_trans_type',record.type);
	    				log.debug('Debug', 'saveRec', 'Plan - ' + planNumber);
	    				
	    				if(override == false){
	    					var c = confirm('Did you want to send this transaction to Wells Fargo? If so press Ok');
	    					if(c == true){
	    						if(isEmptyString(planNumber)){
	    							log.debug('saveRec', 'Inside Empty Plan Number');
	    							alert('Please enter a Plan Number');
	    							return false;
	    						}
	    						if(isEmptyString(transCode)){
	    							log.debug('saveRec', 'Inside Empty Trans Code');
	    							alert('Please enter a Transaction Code');
	    							return false;
	    						}

	    						record.setValue('custbody_send_to_wf',true);

	    						
	    						
	    					}
	    					if(c == false){
	    						return true;
	    					}
	    				}
	    			}
    			}
    			
    		}catch(e){
    			log.error('saveRec', 'Unexpected Error - ' + e.message);
    			alert('Unexpected Error - ' + e.message);
    		}
    		
    		return true;
        }
       
       function getPlan(orderTotal){

//    		var orderTotal = 3455;
    		log.debug('getPlan', 'orderTotal - ' + orderTotal);
    		var plans = [{plan:'4020', 'min':2399.00},
    		             {plan:'1064', 'min':1799.00},
    		             {plan:'1046', 'min':1499.00},
    		             {plan:'1015', 'min':1199.00},
    		             {plan:'1145', 'min':499.00}
    		             ]


    		for(var x=0; x<plans.length; x++){
    		    if (orderTotal >= plans[x].min){
    		    	return  plans[x].plan;
    		    }


    		}
    		
    		if(orderTotal < plans[plans.length-1].min){
    		  	return plans[plans.length-1].plan;
    		}
    		
    		return '';

    	}
       
       function isEmptyString(aValue){
    		//If the values are empty or null run the function
    	   log.debug('isEmptyString', 'Inside empty string');
    		if(aValue == '' || aValue == null){
    			return true;
    		}
    		
    		return false;
    	}

        return {
            saveRecord: saveRec,
            applyForCredit_onClick: applyForCredit_onClick,
            onVerifyAccount_Click: onVerifyAccount_Click
        };
});
