define(['N/record','N/runtime','/SuiteScripts/WellsFargoService-2_0'],
 function(record,runtime,WellsFargo)
 {
	function CreditApp(moduleName,id){
	try{
		//alert('Inside Module ' + moduleName);
		var context = nlapiGetContext();
		var transaction = {};
		var customer = {};
		var so = {};
		var credentials = {};
		
		var nsCustomer = record.load({
			type: record.Type.CUSTOMER,
            id: id,
           })
        var userObj = runtime.getCurrentUser();
		//alert(moduleName);
		customer = WellsFargo.populateCustomer(nsCustomer);
		//alert(JSON.stringify(customer));
		
		credentials.userName = userObj.getPreference({name: "custscript_wf_credit_app_username"});//context.getPreference('custscript_wf_credit_app_username');  //get it from the script record
		credentials.password = userObj.getPreference({name: "custscript_wf_credit_app_password"});//context.getPreference('custscript_wf_credit_app_password');
		credentials.merchantNumber = userObj.getPreference({name: "custscript_wf_merchant_number"});//context.getPreference('custscript_wf_merchant_number');
		credentials.url = userObj.getPreference({name: "custscript_wf_credit_application_url"});//context.getPreference('custscript_wf_credit_application_url');
		
		
		transaction.credentials = credentials;
		transaction.customer = customer;
	
		var returnObject = WellsFargo.WFCreditApplication(transaction);
		alert(returnObject.transactionStatus);
		
		
		if(returnObject.approved){
			record.submitFields({
				 type: record.Type.CUSTOMER,
				 id: id,
				 values: {
					 'custentity_account_number': returnObject.accountNumber,
					 'custentity_transaction_status': returnObject.transactionStatus,
					 'custentity_auth_number': returnObject.authNumber
				 }
			});
			//alert('Customer Id - ' + nlapiGetRecordId());
		}else{
			record.submitFields({
				 type: record.Type.CUSTOMER,
				 id: id,
				 values: {
					 'custentity_transaction_status': returnObject.transactionStatus
				 }
			});
		}
		
		//updateActivityLog(nlapiGetRecordId(),returnObject.transactionStatus,nsCustomer.getFieldValue('terms'));
		location.reload();
	
	
	}catch(e){
		alert('Error - ' + e.message);
		nlapiLogExecution('Debug', 'onCreditApp_Click', 'Error Message - ' + e.message);
	}
}
	
	function onVerifyAccount_Click(moduleName,id){
		try{
			//nlapiLogExecution('Debug', 'onVerifyAccount_Click', 'Button Clicked');
			
			var context = nlapiGetContext();
			var transaction = {};
			var customer = {};
			var so = {};
			var credentials = {};
			
			//var nsCustomer = nlapiLoadRecord('customer', nlapiGetRecordId());
			var nsCustomer = record.load({
				type: record.Type.CUSTOMER,
	            id: id,
	           })
	        var userObj = runtime.getCurrentUser();
			customer = WellsFargo.populateCustomer(nsCustomer,context);
			
			credentials.userName = userObj.getPreference({name: "custscript_wf_credit_app_username"});
			credentials.password = userObj.getPreference({name: "custscript_wf_credit_app_password"});
			credentials.merchantNumber = userObj.getPreference({name: "custscript_wf_merchant_number"});
			credentials.url = userObj.getPreference({name: "custscript_wf_inquiry_service_url"});
			
			transaction.credentials = credentials;
			transaction.customer = customer;
			
			
			var returnObject = WellsFargo.WFSubmitInquiry(transaction);
			                   //WF.WFCreditApplication(transaction);
			
			if(returnObject.approved){
				alert('Account Found, ' + returnObject.transactionStatus);
			}else{
				alert(returnObject.transactionStatus);
			}
		}catch(e){
			alert('Verify account error - ' + e.message);
			nlapiLogExecution('Debug', 'onVerifyAccount_Click', 'Error Message - ' + e.message);
		}
		
	}
	return{
		CreditApp: CreditApp,
		VerifyAccount: onVerifyAccount_Click
	}
 });