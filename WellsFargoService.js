/**************************************************************************
 * 
 * Sends the SOAP request to the bank's API for Credit Application
 * 
 ****************************************************************************/
function WFCreditApplication(transaction){
	var applicationSOAP = createWFCreditApplication(transaction);
	console.log('XML Request - ' + applicationSOAP);
	var header = {"Content-Type" : "text/xml;charset=UTF-8", "SOAPAction" : ""};
	var response = nlapiRequestURL(transaction.credentials.url, applicationSOAP, header);

	console.log('This is the xml response - ' + response.getBody());

	return XMLParse(response);
//	return WFSubmitTransactionParse(response);
	
}


/**************************************************************************
 * 
 * Parses the response from the Credit Application and sets the account
 * number to the account number field on the customer record
 * 
 ****************************************************************************/
function XMLParse(response){
	//alert('Response - ' + response.getBody());
	try{
		var returnObject = {};
		returnObject.approved = false;
		var xmlResponse = nlapiStringToXML(response.getBody());
//		alert('after stringToXML response - ' + nlapiXMLToString(response));
		
		//console.log(response);
	    
	    var path = 'soapenv:Envelope/soapenv:Body/ns1:submitCreditAppResponse/submitCreditAppReturn/transactionStatus';
	    var reasonPath = 'soapenv:Envelope/soapenv:Body/ns1:submitCreditAppResponse/submitCreditAppReturn/faults/faults/faultDetailString';
	    nlapiLogExecution('Debug', 'XMLParse', 'Response Before Select Value - ' + xmlResponse);

	    returnObject.transactionStatusId = nlapiSelectValue(xmlResponse,path);
	    returnObject.reason = nlapiSelectValue(xmlResponse,reasonPath);
	    
	    //alert('xmlData - ' + returnObject.transactionStatusId);
	    
	    if(returnObject.transactionStatusId == 'E0'){
	    	returnObject.transactionStatus = 'Application was Approved';
	    	returnObject.approved = true;
	    }
	    else if(returnObject.transactionStatusId == 'E1'){
	    	returnObject.transactionStatus = 'Application Decision is Delayed';
	    }
	    else if(returnObject.transactionStatusId == 'E2'){
	    	returnObject.transactionStatus = 'Format Error in Application';
	    }
	    else if(returnObject.transactionStatusId == 'E3'){
	    	returnObject.transactionStatus = 'WFF Error';
	    }
	    else if(returnObject.transactionStatusId == 'E4'){
	    	returnObject.transactionStatus = 'Application was Denied';
	    }else{
	    	returnObject.transactionStatus = 'Format Error in Application - ' + returnObject.reason;
	    }
	    
	    var accountPath = 'soapenv:Envelope/soapenv:Body/ns1:submitCreditAppResponse/submitCreditAppReturn/wfAccountNumber';
		returnObject.accountNumber = nlapiSelectValue(xmlResponse,accountPath);
		
	    var authPath = 'soapenv:Envelope/soapenv:Body/ns1:submitCreditAppResponse/submitCreditAppReturn/authorizationNumber';
		returnObject.authNumber = nlapiSelectValue(xmlResponse,authPath);
		    
		return returnObject;
		
	}catch(e){
		nlapiLogExecution('Error', 'XMLParse', 'Unexpected Error - ' + e.message);
		alert('XML Parse Error - ' + e.message);
	}
}

/**************************************************************************
 * 
 * XML fields that will be returned to the WFCreditApplication function to
 * send the SOAP request to the Wells Fargo api.
 * 
 ****************************************************************************/
function createWFCreditApplication(transaction){
	
	var requestString = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://v1.services.webservices.retaildealer.wff.com">' +
	'<soapenv:Header/>' +
	'<soapenv:Body>'+
	'<ns1:submitCreditApp xmlns:ns1="http://services.webservices.retaildealer.wff.com" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
	'<app href="#id0"/>' +
	'</ns1:submitCreditApp>' +
	'<multiRef xmlns:ns2="http://model.webservices.retaildealer.wff.com" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" id="id0" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns2:CreditApp">' +
	'<userName xsi:type="xsd:string">' + transaction.credentials.userName + '</userName>' +
	'<jointFirstName xsi:type="xsd:string">' + transaction.customer.jointFistName + '</jointFirstName>' +
	'<mainPostalCode xsi:type="xsd:string">' + transaction.customer.zip + '</mainPostalCode>' +
	'<jointLastName xsi:type="xsd:string">' + transaction.customer.jointLastName + '</jointLastName>' + 
	'<transactionCode xsi:type="xsd:string">A6</transactionCode>' +
	'<jointAddress1 xsi:type="xsd:string">' + transaction.customer.jointAddress + '</jointAddress1>' +
	'<optionalInsurance xsi:type="xsd:string">' + transaction.customer.optionalInsurance + '</optionalInsurance>' +
	'<jointDOB xsi:type="xsd:string">' + transaction.customer.jointDOB + '</jointDOB>' +
	'<mainCity xsi:type="xsd:string">' + transaction.customer.city + '</mainCity>' +
	'<mainAnnualIncome href="#id2"/>' +
	'<mainEmployerName xsi:type="xsd:string">' + transaction.customer.employerName + '</mainEmployerName>' + 
	'<mainDOB xsi:type="xsd:string">' + transaction.customer.dob + '</mainDOB>' +
	'<uuid xsi:type="xsd:string">' + transaction.customer.entityid + '</uuid>' +
	'<jointStateOrProvince xsi:type="xsd:string">' + transaction.customer.jointState + '</jointStateOrProvince>' +
	'<mainEmployerPhone xsi:type="xsd:string">' + transaction.customer.employerPhone + '</mainEmployerPhone>' +
	'<setupPassword xsi:type="xsd:string">' + transaction.credentials.password + '</setupPassword>' +
	'<merchantNumber xsi:type="xsd:string">' + transaction.credentials.merchantNumber + '</merchantNumber>' +
	'<mainStateOrProvince xsi:type="xsd:string">' + transaction.customer.state + '</mainStateOrProvince>' +
	'<jointSSN xsi:type="xsd:string">' + transaction.customer.jointSSN + '</jointSSN>' +
	'<mainFirstName xsi:type="xsd:string">' + transaction.customer.firstName + '</mainFirstName>' +
	'<individualJointIndicator xsi:type="xsd:string">' + transaction.customer.individualJointIndicator + '</individualJointIndicator>' +
	'<mainLastName xsi:type="xsd:string">' + transaction.customer.lastName + '</mainLastName>' +
	'<checkStatus xsi:type="xsd:string">0</checkStatus>' +
	'<mainAddress1 xsi:type="xsd:string">' + transaction.customer.address + '</mainAddress1>' +
	'<mainHomePhone xsi:type="xsd:string">' + transaction.customer.phone + '</mainHomePhone>' +
	'<jointCity xsi:type="xsd:string">' + transaction.customer.jointCity + '</jointCity>' +
	'<mainSSN xsi:type="xsd:string">' + transaction.customer.ssn + '</mainSSN>' +
	'<jointAnnualIncome href="#id5"/>' +
	'<jointEmployerName xsi:type="xsd:string">' + transaction.customer.jointEmployer + '</jointEmployerName>' +
	'<localeString xsi:type="xsd:string">en_US</localeString>' +
	'<jointEmployerPhone xsi:type="xsd:string">' + transaction.customer.jointEmployerPhone + '</jointEmployerPhone>' +
	'<jointPostalCode xsi:type="xsd:string">' + transaction.customer.jointZip + '</jointPostalCode>' +
	'</multiRef>' +
    '<multiRef xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" id="id5" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="xsd:int">' + transaction.customer.jointAnnualIncome + '</multiRef>' +
    '<multiRef xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" id="id2" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="xsd:int">' + transaction.customer.annualIncome + '</multiRef>' +
//	'</app>' +
//    '</v1:submitCreditApp>' +
	'</soapenv:Body>' +
	'</soapenv:Envelope>';
	
	return requestString;
	
}

/**************************************************************************
 * 
 * Sends the SOAP request to the bank's API for Application Inquiry
 * 
 ****************************************************************************/
function WFSubmitInquiry(transaction){
	var applicationSOAP = createWFInquiry(transaction);
	console.log(applicationSOAP);
	var header = {"Content-Type" : "text/xml;charset=UTF-8", "SOAPAction" : ""};
	var response = nlapiRequestURL(transaction.credentials.url, applicationSOAP, header);
	//nlapiLogExecution('Debug', 'WFSubmitInquiry', 'XML Request - ' + applicationSOAP);
	return InquiryXMLParse(response);
}

/**************************************************************************
 * 
 * Parses the response from the Submit Inquiry and puts the response in
 * an object to be returned to the controller
 * 
 * 
 ****************************************************************************/
function InquiryXMLParse(response){
	console.log('Response - ' + response.getBody());
	try{
		var returnObject = {};
		returnObject.approved = false;
		var xmlResponse = nlapiStringToXML(response.getBody());
		nlapiLogExecution('Debug', 'XMLParse', 'xmlResponse - ' + xmlResponse);
	    
	    var path = 'soapenv:Envelope/soapenv:Body/ns1:submitInquiryResponse/submitInquiryReturn/transactionStatus';
	    var reasonPath = 'soapenv:Envelope/soapenv:Body/ns1:submitInquiryResponse/submitInquiryReturn/sorErrorDescription';
	    
	    returnObject.transactionStatusId = nlapiSelectValue(xmlResponse,path);
	    returnObject.reason = nlapiSelectValue(xmlResponse,reasonPath);
	    
	    //alert('xmlData - ' + returnObject.transactionStatusId);
	    
	    if(returnObject.transactionStatusId == 'I0'){
	    	returnObject.transactionStatus = 'Inquiry Information Follows';
	    	returnObject.approved = true;
	    }
	    if(returnObject.transactionStatusId == 'I1'){
	    	returnObject.transactionStatus = 'Account Not Found - ' + returnObject.reason;
	    }
	    if(returnObject.transactionStatusId == 'I2'){
	    	returnObject.transactionStatus = 'Wells Fargo System Error - ' + returnObject.reason;
	    }
		    
		return returnObject;
	}catch(e){
		nlapiLogExecution('Error', 'XMLParse', 'Unexpected Error - ' + e.message);
		alert('Inquiry Error - ' + e.message);
	}
}

/**************************************************************************
 * 
 * XML fields that will be returned to the createWFInquiry function to
 * send the SOAP request to the Wells Fargo api.
 * 
 ****************************************************************************/
function createWFInquiry(transaction){
	var inquiry = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://services.webservices.retaildealer.wff.com">' +
	'<soapenv:Header/>' +
	'<soapenv:Body>' +
	'<ser:submitInquiry soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
	'<in0 xsi:type="mod:Inquiry" xmlns:mod="http://model.webservices.retaildealer.wff.com">' +
	'<accountNumber xsi:type="xsd:string">' + transaction.customer.accountNumber + '</accountNumber>' + 
	'<merchantNumber xsi:type="xsd:string">' + transaction.credentials.merchantNumber + '</merchantNumber>' +
	'<setupPassword xsi:type="xsd:string">' + transaction.credentials.password + '</setupPassword>' +
	'<transactionCode xsi:type="xsd:string">8</transactionCode>' +
	'<userName xsi:type="xsd:string">' + transaction.credentials.userName + '</userName>' +
	'<uuid xsi:type="xsd:string">' + transaction.customer.entityid + '</uuid>' +
	'</in0>' +
	'</ser:submitInquiry>' +
	'</soapenv:Body>' +
	'</soapenv:Envelope>';
	
	return inquiry;
}

/**************************************************************************
 * 
 * Sends the SOAP request to the bank's API for submitting a transaction
 * 
 ****************************************************************************/
function WFSubmitTransaction(transaction){
	var record = nlapiGetNewRecord();
	var applicationSOAP = createWFTransaction(transaction);
	var header = {"Content-Type" : "text/xml;charset=UTF-8", "SOAPAction" : ""};
	var response = nlapiRequestURL(transaction.credentials.url, applicationSOAP, header);
	nlapiLogExecution('Debug', 'WFSubmitTransaction', 'XML Request - ' + applicationSOAP);
	nlapiLogExecution('Debug', 'WFSubmitTransaction', 'XML Response - ' + response.getBody());
	
	return WFSubmitTransactionParse(response);
}

/**************************************************************************
 * 
 * Parses the response from Submit Transaction and tells the user if the order
 * was approved or denied and puts the response in an object to be returned 
 * to the controller
 * 
 ****************************************************************************/
function WFSubmitTransactionParse(response){
	nlapiLogExecution('Debug', 'WFSubmitTransactionParse', 'response- ' + response.getBody());
	
	try{
		var returnObject = {};
		returnObject.approved = false;
		var xmlResponse = nlapiStringToXML(response.getBody());
		nlapiLogExecution('Debug', 'WFSubmitTransactionParse', 'xmlResponse - ' + xmlResponse);
	    
	    var path = 'soapenv:Envelope/soapenv:Body/ns1:submitTransactionResponse/submitTransactionReturn/transactionStatus';
	    var reasonPath = 'soapenv:Envelope/soapenv:Body/ns1:submitTransactionResponse/submitTransactionReturn/faults/faults/faultDetailString';
	    
	    returnObject.transactionStatusId = nlapiSelectValue(xmlResponse,path);
	    
	    var messagePath = 'soapenv:Envelope/soapenv:Body/ns1:submitTransactionResponse/submitTransactionReturn/transactionMessage';
	    returnObject.transactionMessage = nlapiSelectValue(xmlResponse,messagePath);
	    returnObject.reason = nlapiSelectValue(xmlResponse,reasonPath);
	    
	    //alert('xmlData - ' + returnObject.transactionStatusId);
	    
	    if(returnObject.transactionStatusId == 'A0'){
	    	returnObject.transactionStatus = 'Transaction was Declined - ' + returnObject.transactionMessage;
	    }
	    else if(returnObject.transactionStatusId == 'A1'){
	    	returnObject.transactionStatus = 'Transaction was Approved';
	    	returnObject.approved = true;
	    }
	    else if(returnObject.transactionStatusId == 'A2'){
	    	returnObject.transactionStatus = 'Void Approved No Match Found';
	    }
	    else if(returnObject.transactionStatusId == 'A3'){
	    	returnObject.transactionStatus = 'Void Approved Match Duplicate';
	    }else{
	    	returnObject.transactionStatus = 'Transaction was Declined - ' + returnObject.reason;
	    }
	    
	    var authPath = 'soapenv:Envelope/soapenv:Body/ns1:submitTransactionResponse/submitTransactionReturn/authorizationNumber';
		returnObject.authNumber = nlapiSelectValue(xmlResponse,authPath);
		    
		return returnObject;
	}catch(e){
		nlapiLogExecution('Error', 'XMLParse', 'Unexpected Error - ' + e.message);
		alert('Error - ' + e.message);
	}
}

/**************************************************************************
 * 
 * Sends the SOAP request to the bank's API for Application Inquiry
 * 
 ****************************************************************************/
function createWFTransaction(transaction){
	
	var transactionRequest = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://services.webservices.retaildealer.wff.com">' +
	'<soapenv:Header/>' +
	'<soapenv:Body>' +
	'<ser:submitTransaction soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
	'<in0 xsi:type="mod:Transaction" xmlns:mod="http://model.webservices.retaildealer.wff.com">' +
	'<accountNumber xsi:type="xsd:string">' + transaction.so.accountNumber + '</accountNumber>' +
	'<amount xsi:type="xsd:string">' + transaction.so.amount + '</amount>' +
	'<authorizationNumber xsi:type="xsd:string">' + transaction.so.authNumber + '</authorizationNumber>' +
	'<merchantNumber xsi:type="xsd:string">' + transaction.credentials.merchantNumber + '</merchantNumber>' +
	'<planNumber xsi:type="xsd:string">' + transaction.so.planNumber + '</planNumber>' +
	'<setupPassword xsi:type="xsd:string">' + transaction.credentials.password + '</setupPassword>' +
	//<ticketNumber xsi:type="xsd:string">?</ticketNumber>
	'<transactionCode xsi:type="xsd:string">' + transaction.so.transCode + '</transactionCode>' +
	'<userName xsi:type="xsd:string">' + transaction.credentials.userName + '</userName>' +
	'<uuid xsi:type="xsd:string">' + transaction.so.tranid + '</uuid>' +
	'</in0>' +
	'</ser:submitTransaction>' +
	'</soapenv:Body>' +
	'</soapenv:Envelope>';
	
	return transactionRequest;
}


/************************************************************************
 * Get the values from the NetSuite customer and place them on an 
 * object to be used when transmitting data to Credit company
 * 
 ************************************************************************/
function populateCustomer(nsCustomer,context){
	var phone = nsCustomer.getFieldValue('phone');
	phone = phone.replace(/[- )(]/g,'');
	
	var jointPhone = nsCustomer.getFieldValue('custentity_joint_app_employer_phone');
	if(jointPhone == null){
		jointPhone = '';
	}else{
		jointPhone = jointPhone.replace(/[- )(]/g,'');
	}
	
	
	var employerPhone = nsCustomer.getFieldValue('custentity_employer_phone');
	employerPhone = employerPhone.replace(/[- )(]/g,'');
	
	var dob = nsCustomer.getFieldValue('custentity_date_of_birth');
	dob = getDateFormat(dob);
//	var month = dob.slice(0,1);
//	if(month <= 9)month = '0'+month;
//	var day= dob.slice(3,-5);
//	if(day <= 9)day = '0'+day;
//	var year = dob.slice(-4);
//	
//	dob = month+'/' + day + '/' + year;
	
	var jointDOB = nsCustomer.getFieldValue('custentity_joint_app_dob');
	if(jointDOB == null){
		jointDOB = ''
	}else{
		jointDOB = getDateFormat(jointDOB);
	}
	
	//var name = nsCustomer.getFieldValue('firstname');
var name = nsCustomer.getFieldValue('firstname');
	//name = name.split(' ');
	console.log('name - ' + name);
	
	console.log(nsCustomer.getFieldValue('custentity_joint_app_state'))
	var customer = {};
	customer.firstName = nsCustomer.getFieldValue('firstname');
	customer.lastName = nsCustomer.getFieldValue('lastname');
	customer.checkStatus = '0'; //nsCustomer.getFieldValue('custentity_check_status'); Check date to determine if account number is less than 2 weeks
	customer.dob = dob;
	customer.ssn = context.getSessionObject('SSN');//nsCustomer.getFieldValue('custentity_social_security_number');
	customer.address = nsCustomer.getLineItemValue('addressbook','addr1','1');
	customer.city = nsCustomer.getLineItemValue('addressbook','city','1');
	customer.state = nsCustomer.getLineItemValue('addressbook','dropdownstate_initialvalue','1');
	customer.zip = nsCustomer.getLineItemValue('addressbook','zip','1');
	customer.phone = phone;
	customer.employerName = nsCustomer.getFieldValue('custentity_employer_name');
	customer.employerPhone = employerPhone;
	customer.annualIncome = nsCustomer.getFieldValue('custentity_annual_income');
	customer.jointFistName = isNull(nsCustomer.getFieldValue('custentity_joint_app_first_name'));
	customer.jointLastName = isNull(nsCustomer.getFieldValue('custentity_joint_app_last_name'));
	customer.jointDOB = jointDOB;
	customer.jointSSN = context.getSessionObject('jointSSN');//isNull(nsCustomer.getFieldValue('custentity_joint_app_ssn'));
	customer.jointAddress = isNull(nsCustomer.getFieldValue('custentity_joint_app_address'));
	customer.jointCity = isNull(nsCustomer.getFieldValue('custentity_joint_app_city'));
	customer.jointState = getStateAbbr(nsCustomer.getFieldValue('custentity_joint_app_state'));
	customer.jointZip = isNull(nsCustomer.getFieldValue('custentity_joint_app_zip'));
	customer.jointEmployer = isNull(nsCustomer.getFieldValue('custentity_joint_app_employer'));
	customer.jointEmployerPhone = jointPhone;
	customer.jointAnnualIncome = isNull(nsCustomer.getFieldValue('custentity_joint_app_annual_income'));
	customer.optionalInsurance = '0' //nsCustomer.getFieldValue('custentity_optional_insurance');
	customer.individualJointIndicator = nsCustomer.getFieldValue('custentity_individual_joint_indicator');
	customer.transactionCode = nsCustomer.getFieldValue('custentity_transaction_code');
	customer.accountNumber = nsCustomer.getFieldValue('custentity_account_number');
	customer.entityid = nsCustomer.getFieldValue('entityid');
	
	if(customer.individualJointIndicator == 'T'){
		customer.individualJointIndicator = 'J'
	}
	if(customer.individualJointIndicator == 'F'){
		customer.individualJointIndicator = 'I'
	}
	return customer;
}

function isNull(aValue){
	if(aValue == null){
		aValue = '';
	}
	
	return aValue;
}

function getDateFormat(date){
    var checkMonth = date.substring(0, date.indexOf('/'));
    //alert(checkMonth + ' ' + checkMonth.length);
	if(checkMonth.length == 2){
		var month = date.slice(0,2);
		if(month <= 9)month = '0'+month;
		var day= date.slice(3,-5);
		if(day <= 9)day = '0'+day;
		var year = date.slice(-4);
	
		date = month+'/' + day + '/' + year;
		//console.log(date);
		return date;
	}else{
    	//alert('Inside Else');
    	var month = date.slice(0,1);
		if(month <= 9)month = '0'+month;
		var day= date.slice(2,-5);
		if(day <= 9)day = '0'+day;
		var year = date.slice(-4);
	
		date = month+'/' + day + '/' + year;
		//console.log('else dob - ' + date);
		return date;
	} 
}

/************************************************************************
 * Get the values from the joint applicant state field on the customer
 * and maps it to its state abbreviation.
 * 
 ************************************************************************/
function getStateAbbr(state){
	var abbrs = [{id:'0', 'abbr':'AL'},
     {id:'1', 'abbr':'AK'},
     {id:'2', 'abbr':'AZ'},
     {id:'3', 'abbr':'AR'},
     {id:'4', 'abbr':'CA'},
     {id:'5', 'abbr':'CO'},
     {id:'6', 'abbr':'CT'},
     {id:'7', 'abbr':'DE'},
     {id:'8', 'abbr':'FL'},
     {id:'9', 'abbr':'FL'},
     {id:'10', 'abbr':'GA'},
     {id:'11', 'abbr':'HI'},
     {id:'12', 'abbr':'ID'},
     {id:'13', 'abbr':'IL'},
     {id:'14', 'abbr':'IN'},
     {id:'15', 'abbr':'IA'},
     {id:'16', 'abbr':'KS'},
     {id:'17', 'abbr':'KY'},
     {id:'18', 'abbr':'LA'},
     {id:'19', 'abbr':'ME'},
     {id:'20', 'abbr':'MD'},
     {id:'21', 'abbr':'MA'},
     {id:'22', 'abbr':'MI'},
     {id:'23', 'abbr':'MN'},
     {id:'24', 'abbr':'MS'},
     {id:'25', 'abbr':'MO'},
     {id:'26', 'abbr':'MT'},
     {id:'27', 'abbr':'NE'},
     {id:'28', 'abbr':'NV'},
     {id:'29', 'abbr':'NH'},
     {id:'30', 'abbr':'NJ'},
     {id:'31', 'abbr':'NM'},
     {id:'32', 'abbr':'NY'},
     {id:'33', 'abbr':'NC'},
     {id:'34', 'abbr':'ND'},
     {id:'35', 'abbr':'OH'},
     {id:'36', 'abbr':'OK'},
     {id:'37', 'abbr':'OR'},
     {id:'38', 'abbr':'PA'},
     {id:'39', 'abbr':'PR'},
     {id:'40', 'abbr':'RI'},
     {id:'41', 'abbr':'SC'},
     {id:'42', 'abbr':'SD'},
     {id:'43', 'abbr':'TN'},
     {id:'44', 'abbr':'TX'},
     {id:'45', 'abbr':'UT'},
     {id:'46', 'abbr':'VT'},
     {id:'47', 'abbr':'VA'},
     {id:'48', 'abbr':'WA'},
     {id:'49', 'abbr':'WV'},
     {id:'50', 'abbr':'WI'},
     {id:'51', 'abbr':'WY'},
     ]
	
	for(var x=0; x<abbrs.length; x++){
	    if (state == abbrs[x].id){
	    	console.log('Inside state - ' + state)
	    	return  abbrs[x].abbr;
	    }
	}
	
	return '';
}