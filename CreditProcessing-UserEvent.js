/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 */
define(['N/record','/SuiteScripts/emprove','N/runtime','/SuiteScripts/WellsFargoService-2_0','N/redirect'],
    function (record,emprove,runtime,WF,redirect)
    {
	var PAYMENT_METHOD = '1';
        function onLoad(context)
        {
        	try{
	            if (context.type == context.UserEventType.VIEW){
	            	var customerRecord = context.newRecord;
	            	if(customerRecord.type == 'customer'){
		            	var id = customerRecord.getValue('id');
		            	var terms = customerRecord.getValue('terms');
	
		            	var ef = emprove.init();
		            	log.debug('onBeforeLoad', 'ef - ' + ef);
		            	ef = emprove.useFinance(terms);
		            	
		            	//create the buttons & add the events to run the credit application
			            if (ef){
		            		context.form.clientScriptFileId = 12671;
							var applyBtn = context.form.addButton({
							    id : 'custpage_applybtn',
							    label : 'Apply For Credit',
							    functionName : 'applyForCredit_onClick('+id+','+terms+')'
							});
							
			                
			            }
			            var account = customerRecord.getValue('custentity_account_number'); 
			            if(!(account == null || account == '')){
			            	log.debug('onLoad','account - ' + customerRecord.getValue('custentity_account_number'));
			            	var verifyBtn = context.form.addButton({
							    id : 'custpage_verifybtn',
							    label : 'Verify Account',
							    functionName : 'onVerifyAccount_Click('+id+','+terms+')'
							});
			            }
	            	}
	            }

        	}catch(e){
        		log.error('onLoad','error - ' + e.message);
        	}
           
        }
        /**************************************************************************
         * 
         * Grabs the value on the Sales Order and sends them to the WFSubmitTransaction
         * function to submit the transaction to Wells Fargo.
         * Updates the fields to let user know the status of the transaction.
         * 
         ****************************************************************************/       
        function onBeforeSubmit(context){
        	try{
        		log.debug('onBeforeSubmit','Before Submit Triggered');
        		var rec = context.newRecord;
        		var sessionObj = runtime.getCurrentSession();
        		if(rec.type == 'customer'){			
        			var entityId = rec.getValue('entityid');
        			var ssnSession = sessionObj.get({name: entityId+"SSN"});
        			log.debug('onBeforeSubmit', 'Social - ' + ssnSession);
        			
        			log.debug('onBeforeSubmit', 'Joint Indicator - ' + rec.getValue('custentity_individual_joint_indicator'));//custentity_individual_joint_indicator

        			if(ssnSession == null){
        				//var jointssnSession = context.getSessionObject('jointSSN');
        				var social = rec.getValue('custentity_masked_ssn');
        				//log.debug('onBeforeSubmit', 'Inside ssnSession - ' + social);
        				ssnSession = sessionObj.set({name:entityId+'SSN', value:social});
        				log.debug('onBeforeSubmit','ssn - ' + ssnSession);
        				
        			}
        			var jointssnSession = sessionObj.get({name: entityId+"jointSSN"});
        			log.debug('onBeforeSubmit', 'Joint Social - ' + jointssnSession);
        			if(rec.getValue('custentity_individual_joint_indicator') == true && jointssnSession == null){
        				log.debug('onBeforeSubmit', 'Inside Joint Session');
//        				context.setSessionObject('SSN', social);
        				var jointSocial = rec.getValue('custentity_joint_masked_ssn');
        				jointssnSession = sessionObj.set({name:entityId+'jointSSN', value:jointSocial});
        				log.debug('onBeforeSubmit','Joint ssn - ' + jointssnSession);
        			}
        			
        			rec.setValue('custentity_masked_ssn', '');
        			rec.setValue('custentity_joint_masked_ssn', '');

        		}
        		
        		var send = rec.getValue('custbody_send_to_wf');
        		log.debug('onBeforeSubmit', 'Send - ' + send);
        		
        		if(send == true){
        			
        			log.debug('onBeforeSubmit', 'Inside Send Is True');
        			var session = sessionObj.get({name: "CREDIT_CHECK"});
        			var transaction = {};
        		//	var customer = {};
        			var so = {};
        			var credentials = {};
        			var transCode = rec.getValue('custbody_transaction_code');
        			var terms = rec.getValue('terms');
        			log.debug('onBeforeSubmit', 'session - ' + session);
        			var recType = rec.type;

        			if(!isEmptyString(transCode)){
        			if(recType == 'salesorder' && transCode == '1' || recType == 'salesorder' && transCode == '3' || recType == 'invoice' && transCode == '1' || recType == 'invoice' && transCode == '3'){
        				var nsSalesOrder = context.newRecord;
        				so.amount = nsSalesOrder.getValue('total');
        				so.authNumber = nsSalesOrder.getValue('custbody_authorization_number');
        				so.planNumber = nsSalesOrder.getValue('custbody_credit_app_plan_number');
        				so.transCode = nsSalesOrder.getValue('custbody_transaction_code');
        				so.accountNumber = nsSalesOrder.getValue('custbody_account_number');
        				so.override = nsSalesOrder.getValue('custbody_manual_override');
        				so.tranid = nsSalesOrder.getValue('tranid');
        				
        				if(so.transCode == '1'){
        					so.authNumber = '000000';
        				}
        				else if(so.transCode == '2'){
        					so.transCode = '3';
        					so.authNumber = nsSalesOrder.getValue('custbody_authorization_number');
        				}
        				else if(so.transCode == '3'){
        					so.transCode = '5';
        					so.authNumber = '000000';
        				}
        				
        				log.debug('onBeforeSubmit', 'Auth Number Before Response - ' + so.authNumber);
        				
        				transaction.so = so;
        				log.debug('onBeforeSubmit', 'Object - ' + JSON.stringify(so));
        				/*
        				var nsCustomer = nlapiLoadCustomer(nsSalesOrder.getFieldValue('entity'));
        				customer = populateCustomer(nsCustomer);
        				*/
        			
        				var userObj = runtime.getCurrentUser();
        				credentials.userName = userObj.getPreference({name: "custscript_wf_credit_app_username"});
        				credentials.password = userObj.getPreference({name: "custscript_wf_credit_app_password"});
        				credentials.merchantNumber = userObj.getPreference({name: "custscript_wf_merchant_number"});
        				credentials.url = userObj.getPreference({name: "custscript_wf_submit_transaction_url"});
        				transaction.credentials = credentials;
        				var returnObject = WF.WFSubmitTransaction(transaction);
        				
        				//log.debug('onBeforeSubmit','returnObject - ' + JSON.stringify(returnObject));
        				log.debug('onBeforeSubmit','returnObject.transactionStatus - ' + returnObject.transactionStatus);
        				log.debug('onBeforeSubmit', 'Transaction Status - ' + returnObject.transactionStatus);
        				log.debug('onBeforeSubmit', 'Auth Number - ' + returnObject.authNumber);
        				log.debug('onBeforeSubmit', 'Trans Id - ' + returnObject.transactionStatusId);
        		
        				rec.setValue('custbody_transaction_status', returnObject.transactionStatus);
        				rec.setValue('custbody_authorization_number', returnObject.authNumber);
        				
        				
        				if(returnObject.transactionStatusId == 'A1' && recType == 'invoice'){
        					log.debug('onBeforeSubmit', 'Inside Approved Session');
        					//context.setSessionObject('CREDIT_CHECK', 'Approved');
        					sessionObj.set({name:'CREDIT_CHECK', value:'Approved'});
        					nsSalesOrder.setValue('custbody_transaction_code', null);
        				}

        				//updateTransActivityLog(nlapiGetRecordId(),returnObject.transactionStatus,terms);
        	
        			}
        			}
        			
        			rec.setValue('custbody_send_to_wf',false);
        			
        		}
        	}catch(e){
        		log.error('onBeforeSubmit','error - ' + e.message);
        	}
        }
        
        /**************************************************************************
         * 
         * If the type is invoice and the transaction code is authorize or
         * authorize and charge, send the invoice to Wells Fargo and then
         * transform the invoice to a customer payment.
         * 
         ****************************************************************************/
        function onAfterSubmit(context){
        	var rec = context.newRecord;
        	var sessionObj = runtime.getCurrentSession();			
    		var session = sessionObj.get({name: "CREDIT_CHECK"});
        	log.debug('onAfterSubmit', 'session - ' + session);
        	
        	if(rec.type == 'invoice'){
        			log.debug('Inside rec type is invoice')
        			if(session == 'Approved'){
        				log.debug('onAfterSubmit', 'id - ' + rec.getValue('id'));
        				var department = rec.getValue('department');
        				var invoiceClass = rec.getValue('class');
        				var transCode = rec.getValue('custbody_transaction_code');
        				var status = rec.getValue('custbody_transaction_status');
        				var salesRep = rec.getValue('salesrep');
        				var terms = rec.getValue('terms');
        				var total = rec.getValue('total');
        				
        				log.debug('onAfterSubmit', 'Transaction Code - ' + transCode);
        				try{
        					log.debug('onAfterSubmit', 'Inside if approved statement');
//        					ctx.setSessionObject('CREDIT_CHECK', null);
        					sessionObj.set({name:'CREDIT_CHECK', value:null});
        					log.debug('onAfterSubmit', 'before record transform');
        					//var customerPayment = nlapiTransformRecord('invoice', record.getId(), 'customerpayment');
        					var customerPayment = record.transform({
        					    fromType: record.Type.INVOICE,
        					    fromId: rec.getValue('id'),
        					    toType: record.Type.CUSTOMER_PAYMENT,
        					    isDynamic: true,
        					});
        					customerPayment.setValue('department', department);
        					customerPayment.setValue('class', invoiceClass);
        					customerPayment.setValue('payment', total);
        					customerPayment.setValue('paymentmethod', PAYMENT_METHOD);
        							
        					var submit = customerPayment.save();//nlapiSubmitRecord(customerPayment, true);
        					log.debug('onAfterSubmit', 'Customer Payment - ' + submit);
        					//nlapiSetRedirectURL('RECORD', 'customerpayment', submit, true);
        		        	redirect.toRecord({
        		          		 type:  record.Type.CUSTOMER_PAYMENT,
        		          		 id: submit,
        		          		 isEditMode: false
        		           	});
        	
        				}catch(e){
        					log.error('onAfterSubmit', 'Error Message - ' + e.message);
        				}
        			}
        	}
        }
        
        function isEmptyString(aValue){
    		//If the values are empty or null run the function
    	   log.debug('isEmptyString', 'Inside empty string');
    		if(aValue == '' || aValue == null){
    			return true;
    		}
    		
    		return false;
    	}
        
        return {
            beforeLoad: onLoad,
            beforeSubmit: onBeforeSubmit,
            afterSubmit: onAfterSubmit
            
        };
    });

