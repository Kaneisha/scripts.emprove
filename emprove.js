define(['/SuiteScripts/Search-20','N/search','N/record','/SuiteScripts/CreditProcessingController-2_0'],
    function (searchScript,search,record,creditProcessor){
    	var financialOrgs = null;
    	var ids = null;
    	function init(term){
    		//returns as a JSON object
    		financialOrgs = searchConfig(term);
    		//financialOrgs = useFinance(term);
    		return financialOrgs;
    	}

    	function useFinance(term){
    		//loops thru financialOrgs to find the term
    		//retuns true or false
    		log.debug('useFinance','term - ' + term);
    		log.debug('useFinance','financialOrgs - ' + JSON.stringify(financialOrgs));
    		for(var i=0; i<financialOrgs.length; i++){
    			log.debug('useFinance','financialOrgs - ' + JSON.stringify(financialOrgs));
    		    if (term == financialOrgs[i]){
    		    	return  true;
    		    }
    		    
    		    return false;


    		}
    		//
    	}
    	//var getFinanceOrgModule = function(term,id,type){
    	function getFinanceOrgModule(term,id,type){
    		//loop thru finacialOrgs to find the term
    		var moduleName = searchConfig(term);
    		return moduleName;
//    		if(type == 'creditApp'){
//    			var module = creditProcessor.CreditApp(moduleName,id);
//    		}else{
//    			var module = creditProcessor.VerifyAccount(moduleName,id);
//    		}
    		
//    		return module;
	        //return moduleName;

    	}
    	function searchConfig(term){
    		try{
    			//alert('hello');
	    		var columns = [
	    		     search.createColumn({
		    			 name: 'custrecord_em_config_term'
	    		     }),
	    		     search.createColumn({
		    			 name: 'custrecord_emp_config_module'
	    		     })
	    		];
	    		var filters = [
					search.createFilter({
						name : 'custrecord_em_config_term',
		        		operator : search.Operator.ANYOF,
		        		values : term
					})            
	    		];
	    		if(term == undefined){
	    			var newSearch = searchScript.init('customrecord_em_configuration',null,null,columns);
	    			var searchResults = newSearch.getResults();	
	    		}else{
	    			var newSearch = searchScript.init('customrecord_em_configuration',null,filters,columns);
	    			var searchResults = newSearch.getResults();
	    			return searchResults[0].getValue('custrecord_emp_config_module');
	    			
	    		}
	    		
	           
	            //log.debug('searchConfig',searchResults);
	            var terms = [];
	           
	            for(var i=0; i < searchResults.length; i++){
					terms.push(searchResults[i].getValue('custrecord_em_config_term'));
					//terms.push(termsObj);
				}
	            log.debug('searchConfig',terms);
    		}catch(e){
    			log.error('searchConfig','error - ' + e.message)
    		}
            	return terms;
    	}
    	
    	return{
    		init: init,
    		useFinance: useFinance,
    		getFinanceOrgModule: getFinanceOrgModule
    	}
});