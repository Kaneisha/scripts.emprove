function updateActivityLog(customerId,status,terms){
	
	try{
			
		var filters = [];
		filters[0] = new nlobjSearchFilter('internalid', 'custrecord_em_customer_id', 'anyof', customerId);
		
		var search = new Search('customrecord_emprove_activity_log',null,filters);
		var searchResults = search.getResults();
	
		if (searchResults == null || searchResults.length < 1){
			nlapiLogExecution('Debug','updateActivityLog', 'Empty search results');
			createActivityLog(customerId,terms,status,'customer');
			return;
		}
		

		var recordId = searchResults[0].getId();
		var activityLog = nlapiLoadRecord('customrecord_emprove_activity_log',recordId);
		
		activityLog.setFieldValue('custrecord_em_financial_service',terms);
		activityLog.setFieldValue('custrecord_em_results',status);
		activityLog.setFieldValue('custrecord_em_date_updated',formatDate());
		
		nlapiSubmitRecord(activityLog);
		
	}catch(e){
		//alert('Error - ' + e.message);
		nlapiLogExecution('Error', 'updateActivityLog', 'Error - ' + e.message);
	}
}

function updateTransActivityLog(tranId,status,terms){
	try{
		nlapiLogExecution('Debug','updateTransActivityLog', 'tranId - ' + tranId);
		var filters = [];
		filters[0] = new nlobjSearchFilter('internalid', 'custrecord_em_transaction_id', 'anyof', tranId);
		
		var search = new Search('customrecord_emprove_activity_log_trans',null,filters);
		var searchResults = search.getResults();
		nlapiLogExecution('Debug','updateTransActivityLog', 'searchResults - ' + searchResults);
	
		if (searchResults == null || searchResults.length < 1){
			nlapiLogExecution('Debug','updateTransActivityLog', 'Empty search results');
			createActivityLog(tranId,terms,status,'transaction');
			return;
		}
		

		var recordId = searchResults[0].getId();
		var activityLog = nlapiLoadRecord('customrecord_emprove_activity_log_trans',recordId);
		
		activityLog.setFieldValue('custrecord_em_financial_service_trans',terms);
		activityLog.setFieldValue('custrecord_em_results_trans',status);
		activityLog.setFieldValue('custrecord_em_date_updated_trans',formatDate('transaction'));
		
		nlapiSubmitRecord(activityLog);
		
	}catch(e){
		//alert('Error - ' + e.message);
		nlapiLogExecution('Error', 'updateActivityLog', 'Error - ' + e.message);
	}
}

function createActivityLog(recId,terms,status,recType){
	try{
		nlapiLogExecution('Debug','createActivityLog', 'RecId - ' + recId);
		if(recType == 'customer'){
			var record = nlapiCreateRecord('customrecord_emprove_activity_log');
			record.setFieldValue('custrecord_em_customer_id',recId);
			record.setFieldValue('custrecord_em_financial_service',terms);
			record.setFieldValue('custrecord_em_results',status);
			record.setFieldValue('custrecord_em_date_updated',formatDate());
		}else{
			var record = nlapiCreateRecord('customrecord_emprove_activity_log_trans');
			record.setFieldValue('custrecord_em_transaction_id',recId);
			record.setFieldValue('custrecord_em_financial_service_trans',terms);
			record.setFieldValue('custrecord_em_results_trans',status);
			record.setFieldValue('custrecord_em_date_updated_trans',formatDate('transaction'));
		}
		
		var submit = nlapiSubmitRecord(record);
		nlapiLogExecution('Debug','createActivityLog', 'Record Submitted - ' + submit);
	}catch(e){
		nlapiLogExecution('Error', 'createActivityLog', 'Error - ' + e.message);
	}
	
	
}

function formatDate(recType){
	
	nlapiLogExecution('Debug', 'formatDate', 'RecType - ' + recType);
	if(recType == 'transaction'){
		nlapiLogExecution('Debug', 'formatDate', 'Inside Transaction Format Date');
		var d = new Date();
		var month = d.getMonth() + 1;
		var year = d.getFullYear();
		var day = d.getDate();
		var date = month+'/'+day+'/'+year;
		var hours = d.getHours();
		var min = d.getMinutes();
		var sec = d.getSeconds();
		var hours = (hours+24-2)%24; 
		var mid='am';
		if(hours==0){ //At 00 hours we need to show 12 am
		    hours=12;
		}else if(hours>12){
		    hours=hours%12;
		    mid='pm';
		}
		date = date + ' ' + hours + ':' + min + ':' + sec + ' ' + mid;
	}else{
		var date = new Date();
		date = date.toLocaleString();
		date = date.replace(',','');
	}
	nlapiLogExecution('Debug', 'formatDate', 'Date - ' + date);
	return date;
}